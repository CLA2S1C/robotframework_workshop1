***Settings***
Library  BuiltIn  #📕เรียกใช้ Library BuiltIn สำหรับจัดการเรื่องตัวแปร การ Convert | Evaluate ฯฯ
***Variables***
${radius} =  6  #📗รัศมีวงกลม
***Test Cases***
1️⃣ คำนวนหาพื้นที่วงกลมโดยใช้รัศมีวงกลม(เมตร) 1️⃣
    ${result} =  CircleArea  ${radius}  #📘ประกาศตัวแปร result เพื่อเก็บผลลัพธ์ที่ได้จาก func
    Set Global Variable  ${result}  #📙เซตให้ค่าเป็น Global เพื่อนำไปใช้แสดงผลได้
2️⃣ แสดงผลพื้นที่วงกลมที่ได้ออกมา 2️⃣
    ${formatResult} =  Convert To Number  ${result}  2  #📒กำหนดรูปแบบให้มี ทศนิยม 2 ตำแหน่ง
    Log To Console  \n✅ผลลัพธ์: ${formatResult} ตารางเมตร✅  #📦แสดงผลลัพธ์ออกมา
***Keywords***
CircleArea
    [Arguments]  ${paramRadius}  #🖍รับตัวรัศมีวงกลมเพื่อนำมาคำนวน
    ${Calculate} =  Evaluate  (22/7) * (${paramRadius} * ${paramRadius})  #✏️คำนวนหาพื้นที่วงกลม π = (22/7) | 3.14
    [Return]  ${Calculate}  #😁ส่งค่าที่คำนวนได้กลับออกมา